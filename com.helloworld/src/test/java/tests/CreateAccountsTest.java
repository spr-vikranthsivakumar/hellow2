package tests;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.IOException;

import org.testng.Assert;
import org.testng.asserts.*;
import org.testng.asserts.SoftAssert;

import pages.LandingPage;
import pages.LoginPage;
import pages.VerificationPage;
import pages.AccountDetailsPage;
import pages.CreateAccountPage;
import utilities.Credentials;
import utilities.Waits;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;

import java.util.Iterator;
import java.util.UUID;

public class CreateAccountsTest extends BaseTest {
	
	//@AfterMethod
	public void deleteStuff() throws Exception{
		AccountDetailsPage adp = new AccountDetailsPage(driver).waitForPage(driver);
		adp.deleteAccount();
	}
	

	@Test
	public void completeAll() throws Exception{
		//Fill Other Customer
		loginCredentials(Credentials.sandboxLogin());
		visit("https://cs52.salesforce.com/001/e?retURL=%2F001%2Fo");
		CreateAccountPage acc = new CreateAccountPage(driver).waitForPage(driver);
		acc.fillOtherCustomerInfo();
		AccountDetailsPage adp = new AccountDetailsPage(driver).waitForPage(driver);
		softAssert.assertEquals(acc.verifyOExcelAccountName(), adp.verifyAccountName());
		softAssert.assertEquals(acc.verifyOExcelAccountPhone(), adp.verifyAccountPhoneNumber());
		softAssert.assertTrue(adp.verifyAccountBillingAddress().contains(acc.verifyOExcelAccountAddress()));
		softAssert.assertAll();
		adp.deleteAccount();

		//Fill Partner Customer
		visit("https://cs52.salesforce.com/001/e?retURL=%2F001%2Fo");
		acc.fillPartnerInformation();
		AccountDetailsPage adp2 = new AccountDetailsPage(driver).waitForPage(driver);
		softAssert.assertEquals(acc.verifyPExcelAccountName(), adp2.verifyAccountName());
		softAssert.assertEquals(acc.verifyPExcelAccountPhone(), adp2.verifyAccountPhoneNumber());
		softAssert.assertTrue(adp2.verifyAccountBillingAddress().contains(acc.verifyPExcelAccountAddress()));
		softAssert.assertAll();
		adp2.deleteAccount();
		
		//Fill past Customer
		visit("https://cs52.salesforce.com/001/e?retURL=%2F001%2Fo");
		acc.pastCustomerInfo();
		AccountDetailsPage adp3 = new AccountDetailsPage(driver).waitForPage(driver);
		softAssert.assertEquals(acc.verifyPSTExcelAccountName(), adp.verifyAccountName());
		softAssert.assertEquals(acc.verifyPSTExcelAccountPhone(), adp.verifyAccountPhoneNumber());
		softAssert.assertTrue(adp.verifyAccountBillingAddress().contains(acc.verifyPSTExcelAccountAddress()));
		softAssert.assertAll();
		adp3.deleteAccount();
		
	}
	
	@Test 
	public void iterateRowTest() throws Exception{
		visit("https://cs52.salesforce.com/001/e?retURL=%2F001%2Fo");
		CreateAccountPage acc = new CreateAccountPage(driver).waitForPage(driver);
	    Iterator<Row> rowIterator = sheet.iterator();

	    while(rowIterator.hasNext()) {
	        Row row = rowIterator.next();

	    	
	    	Iterator<Cell> cellIterator = row.cellIterator();
	        while(cellIterator.hasNext()) {        

	            Cell cell = cellIterator.next();
	            if(cell.getColumnIndex() == 0){
	                     driver.findElement(By.name("UserName")).sendKeys(cell.getStringCellValue());
	            }
	            else
	                driver.findElement(By.name("Password")).sendKeys(cell.getStringCellValue());
	        }
	    }

		
	}
	

	
	//Create Account Type: Other
	@Test()
	public void createAccontOther() throws Exception {
		
		loginCredentials(Credentials.sandboxLogin());
		visit("https://cs52.salesforce.com/001/e?retURL=%2F001%2Fo");
		CreateAccountPage acc = new CreateAccountPage(driver).waitForPage(driver);
		acc.fillOtherCustomerInfo();
		AccountDetailsPage adp = new AccountDetailsPage(driver).waitForPage(driver);

		softAssert.assertEquals(acc.verifyOExcelAccountName(), adp.verifyAccountName());
		softAssert.assertEquals(acc.verifyOExcelAccountPhone(), adp.verifyAccountPhoneNumber());
		softAssert.assertTrue(adp.verifyAccountBillingAddress().contains(acc.verifyOExcelAccountAddress()));
		softAssert.assertAll();
		
		deleteStuff();
		
	}

	//Create Account Type: Partner
	@Test()
	public void createAccountPartner() throws Exception {
		
		loginCredentials(Credentials.sandboxLogin());
		visit("https://cs52.salesforce.com/001/e?retURL=%2F001%2Fo");
		CreateAccountPage acc = new CreateAccountPage(driver).waitForPage(driver);
		acc.fillPartnerInformation();
		AccountDetailsPage adp = new AccountDetailsPage(driver).waitForPage(driver);

		softAssert.assertEquals(acc.verifyPExcelAccountName(), adp.verifyAccountName());
		softAssert.assertEquals(acc.verifyPExcelAccountPhone(), adp.verifyAccountPhoneNumber());
		softAssert.assertTrue(adp.verifyAccountBillingAddress().contains(acc.verifyPExcelAccountAddress()));
		softAssert.assertAll();
		
	}
	
	//Create Account Type: Past Customer
	@Test()
	public void createAccountPastCustomer() throws Exception {
		
		loginCredentials(Credentials.sandboxLogin());
		visit("https://cs52.salesforce.com/001/e?retURL=%2F001%2Fo");
		CreateAccountPage acc = new CreateAccountPage(driver).waitForPage(driver);
		acc.pastCustomerInfo();
		AccountDetailsPage adp = new AccountDetailsPage(driver).waitForPage(driver);

		softAssert.assertEquals(acc.verifyPSTExcelAccountName(), adp.verifyAccountName());
		softAssert.assertEquals(acc.verifyPSTExcelAccountPhone(), adp.verifyAccountPhoneNumber());
		softAssert.assertTrue(adp.verifyAccountBillingAddress().contains(acc.verifyPSTExcelAccountAddress()));
		softAssert.assertAll();

	}
	
	
	}


