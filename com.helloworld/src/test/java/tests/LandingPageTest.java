package tests;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.IOException;

import org.testng.Assert;
import org.testng.asserts.*;

import utilities.Credentials;
import utilities.Waits;

import java.util.UUID;

public class LandingPageTest extends BaseTest {
	
	@Test
	public void verifyLandingPage() throws Exception{
		System.out.println(driver.getCurrentUrl());
		Assert.assertEquals(driver.getCurrentUrl(), "https://test.salesforce.com/");
	}
	
}
