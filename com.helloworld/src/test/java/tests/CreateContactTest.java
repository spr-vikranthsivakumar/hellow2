package tests;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.IOException;

import org.testng.Assert;
import org.testng.asserts.*;
import org.testng.asserts.SoftAssert;

import pages.LandingPage;
import pages.LoginPage;
import pages.RecentAccountsPage;
import pages.RecentContactsPage;
import pages.SelectContactTypePage;
import pages.VerificationPage;
import pages.AccountDetailsPage;
import pages.BasePage;
import pages.ContactDetailsPage;
import pages.CreateAccountPage;
import pages.CreateContactPage;
import utilities.Credentials;
import utilities.Waits;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.UUID;

public class CreateContactTest extends BaseTest {
	
	@BeforeMethod
	public void setupStuff() throws Exception{
		setupAccountData();
	}
	@AfterMethod
	public void deleteStuff() throws Exception{
		deleteAccountData();
	}
			
	//Create New Contact
		@Test(priority = 0)
		public void createNewContact() throws IOException {
			
			LandingPage lp = new LandingPage(driver);
			try {
				lp.createNewDropdownContact();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SelectContactTypePage sct = new SelectContactTypePage(driver).waitForPage(driver);
			try {
				sct.clickContinueButton();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			CreateContactPage ccp = new CreateContactPage(driver);
			try {
				ccp.fillContactInfo();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ContactDetailsPage cdp = new ContactDetailsPage(driver).waitForPage(driver);
			
			softAssert.assertEquals(ccp.verifyCFullName(), cdp.verifyFullName());
			softAssert.assertEquals(ccp.verifyCExcelAccountName(), cdp.verifyAccountName());
			softAssert.assertAll();
			
		}
	
}

