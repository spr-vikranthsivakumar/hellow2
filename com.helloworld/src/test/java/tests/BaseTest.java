package tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;

import pages.AccountDetailsPage;
import pages.CreateAccountPage;
import pages.LoginPage;
import pages.RecentAccountsPage;
import utilities.Config;
import utilities.Credentials;
import utilities.Utilities;

public class BaseTest {

	public static WebDriver driver;
	public static WebDriverWait wait;
	public static String testName;
	public static String url;
	public static String user;
	public static String password;
	public static String IDNum;
	public static boolean tos;

	HSSFWorkbook workbook;
	HSSFSheet sheet;
	HSSFCell cell;
	SoftAssert softAssert = new SoftAssert();

	@BeforeClass
	public void setDriverLocation() throws EncryptedDocumentException, InvalidFormatException, IOException {
		if (SystemUtils.IS_OS_WINDOWS) {
			System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
			System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");
		} else {
			System.setProperty("webdriver.chrome.driver", "drivers/chromedriver");
			System.setProperty("webdriver.gecko.driver", "drivers/geckodriver");
		}
	}

	@BeforeMethod
	public void setup(Method method) throws Exception {
		console("Setting up test: " + method.getName());

		driver = Config.setDriver("chrome");
		Config.inSandBoxEnvironment();

		File src = new File("./Workbook1.xls");
		FileInputStream finput = new FileInputStream(src);
		workbook = new HSSFWorkbook(finput);
		sheet = workbook.getSheetAt(0);
		
		//Date date = new Date();
		//SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
		//String formattedDate = sdf.format(date);
		//System.out.println(formattedDate); // 12/01/2011 4:48:16 PM
	}

	/**
	 * Standard tear down with screenshot on test fail
	 * 
	 * @param testResult
	 * @throws IOException
	 *             //
	 */
	@AfterMethod
	public void tearDown(ITestResult testResult, Method method) throws IOException {
		String date = Utilities.getDate(0, "HH:mm__MM.dd.yyyy");
		if (testResult.getStatus() == ITestResult.FAILURE) {
			Utilities.screenGrab(method.getName() + "-" + date);

		}

		if (driver != null) {
			driver.quit();
		}
	}

	public static void setCredentials(String[] credentials) {

		Credentials.setCredentials(credentials);

		user = Credentials.userLogin();
		System.out.println(user);
		password = Credentials.userPassword();
		System.out.println(password);

	}

	public static LoginPage loginCredentials(String[] credentials) throws Exception {
		setCredentials(credentials);
		LoginPage log = new LoginPage(driver).waitForPage(driver);
		log.loginMethod(user, password);
		return log;

	}

	public static void visit(String url) {
		console("Navigating to: " + url);
		driver.get(url);
	}

	protected static void console(Object text) {
		System.out.println(text);
	}
	
	public static void setupAccountData() throws Exception{
	    loginCredentials(Credentials.sandboxLogin());
	    visit("https://sprconsulting--autotest1.cs17.my.salesforce.com/001/e?retURL=%2F001%2Fo");
		CreateAccountPage AP = new CreateAccountPage(driver);
	    AP.fillOtherCustomerInfo();
	    visit("https://sprconsulting--autotest1.cs17.my.salesforce.com/");
		}
	
	public static void deleteAccountData() throws Exception{
		visit("https://sprconsulting--autotest1.cs17.my.salesforce.com/001/o");
		RecentAccountsPage rap = new RecentAccountsPage(driver);
		rap.selectMostRecentAccount();		
		AccountDetailsPage adp = new AccountDetailsPage(driver).waitForPage(driver);
		adp.deleteAccount();
		

		//Following lines will delete a contact ONLY- by default, the previous steps to delete an account will delete the associated contact as well
		
		//visit("https://sprconsulting--autotest1.cs17.my.salesforce.com/003/o");
		//RecentContactsPage rcp = new RecentContactsPage(driver).waitForPage(driver);
		//rcp.selectMostRecentContact();
		//ContactDetailsPage cdp = new ContactDetailsPage(driver).waitForPage(driver);
		//cdp.deleteContact();
	}
}