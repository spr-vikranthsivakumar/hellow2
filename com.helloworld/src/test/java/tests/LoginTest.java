package tests;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.*;

import pages.LandingPage;
import pages.LoginPage;
import pages.VerificationPage;
import pages.CreateAccountPage;
import utilities.Credentials;
import utilities.Waits;


import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell
;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.UUID;

public class LoginTest extends BaseTest {
	@Test
	public void loginTest() throws Exception{
		loginCredentials(Credentials.sandboxLogin());
		LandingPage land = new LandingPage(driver).waitForPage(driver);
		Assert.assertEquals(driver.getCurrentUrl(), land.getURL());
		
	}
	 
	
	
}



