package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Waits;

public class SelectContactTypePage extends BasePage {

	public SelectContactTypePage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	String title = "New Contact: Select Contact Record Type ~ Salesforce - Enterprise Edition";

	public SelectContactTypePage waitForPage(WebDriver driver) {
		while (!driver.getTitle().contains(title)) {
			Waits.pause(1);
			System.out.println("Waiting for Sales Force Contact Record Type page to load..." + driver.getTitle());
		}
		System.out.println("Sales Force Select Contact Record Type Loaded");
		return PageFactory.initElements(driver, SelectContactTypePage.class);
	}

	// Elements
	By continueButton = By.xpath("//*[@id=\"bottomButtonRow\"]/input[1]");
	By typeDropDown = By.id("p3");
	By typeContact = By.cssSelector("#p3>option:nth-of-type(1)");
	
	// Method for Selecting Contact Record Type
	public void selectTypeContactAndContinue() {
		driver.findElement(typeDropDown).click();
		driver.findElement(typeContact).click();
		Waits.pause(2);
		driver.findElement(continueButton).click();
		Waits.pause(2);
	}
	// Method for Clicking Continue Button
	public void clickContinueButton() throws Exception {
		
		Waits.pause(5);
		driver.findElement(continueButton).click();
		Waits.pause(1);

	}
}