package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Waits;

public class CreateAccountPage extends BasePage {

	public CreateAccountPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	String title = "Account Edit: New Account ~ Salesforce - Enterprise Edition";

	public CreateAccountPage waitForPage(WebDriver driver) {
		while (!driver.getTitle().contains(title)) {
			Waits.pause(1);
			System.out.println("Waiting for Sales Force Account page to load..." + driver.getTitle());
		}
		System.out.println("Sales Force Landing Page Loaded");
		return PageFactory.initElements(driver, CreateAccountPage.class);
	}

	// Elements
	By saveButton = By.cssSelector("#bottomButtonRow>input:nth-of-type(1)");
	By accountTypeDropDown = By.cssSelector("#acc6");
	By accountOther = By.cssSelector("#acc6>option:nth-of-type(3)");
	By accountPartner = By.cssSelector("#acc6>option:nth-of-type(4)");
	By accountPastCustomer = By.cssSelector("#acc6>option:nth-of-type(6)");

	// Account Fields
	By accountName = By.cssSelector("#acc2");
	By phoneField = By.cssSelector("#acc10");
	By billingStreet = By.cssSelector("#acc17street");
	By billingCity = By.cssSelector("#acc17city");
	By billingState = By.cssSelector("#acc17state");

	// Methods for Selecting Customer Type
	public void selectCustomerOther() {
		driver.findElement(accountTypeDropDown).click();
		driver.findElement(accountOther).click();

	}

	public void selectCustomerPartner() {
		driver.findElement(accountTypeDropDown).click();
		driver.findElement(accountPartner).click();
	}

	public void selectPastCustomer() {
		driver.findElement(accountTypeDropDown).click();
		driver.findElement(accountPastCustomer).click();
	}

	// Iteration for grabbing Data
	public void fillOtherCustomerInfo() throws Exception {
		selectCustomerOther();
		File src = new File("./Workbook1.xls");
		FileInputStream finput = new FileInputStream(src);
		workbook = new HSSFWorkbook(finput);
		sheet = workbook.getSheetAt(0);

		// Account Name
		cell = sheet.getRow(0).getCell(0);
		driver.findElement(accountName).clear();
		driver.findElement(accountName).sendKeys(cell.getStringCellValue()+" "+formattedDate);

		// Phone Number
		cell = sheet.getRow(0).getCell(1);
		driver.findElement(phoneField).clear();
		driver.findElement(phoneField).sendKeys(cell.getStringCellValue());

		// State
		cell = sheet.getRow(0).getCell(2);
		driver.findElement(billingState).clear();
		driver.findElement(billingState).sendKeys(cell.getStringCellValue());

		// Street
		cell = sheet.getRow(0).getCell(3);
		driver.findElement(billingStreet).clear();
		driver.findElement(billingStreet).sendKeys(cell.getStringCellValue());

		// City
		cell = sheet.getRow(0).getCell(4);
		driver.findElement(billingCity).clear();
		driver.findElement(billingCity).sendKeys(cell.getStringCellValue());

		scrollAndClick(find(saveButton),false);

	}

	public void fillPartnerInformation() throws Exception {
		selectCustomerPartner();
		File src = new File("./Workbook1.xls");
		FileInputStream finput = new FileInputStream(src);
		workbook = new HSSFWorkbook(finput);
		sheet = workbook.getSheetAt(0);

		// Account Name
		cell = sheet.getRow(1).getCell(0);
		driver.findElement(accountName).clear();
		driver.findElement(accountName).sendKeys(cell.getStringCellValue());

		// Phone Number
		cell = sheet.getRow(1).getCell(1);
		driver.findElement(phoneField).clear();
		driver.findElement(phoneField).sendKeys(cell.getStringCellValue());

		// State
		cell = sheet.getRow(1).getCell(2);
		driver.findElement(billingState).clear();
		driver.findElement(billingState).sendKeys(cell.getStringCellValue());

		// Street
		cell = sheet.getRow(1).getCell(3);
		driver.findElement(billingStreet).clear();
		driver.findElement(billingStreet).sendKeys(cell.getStringCellValue());

		// City
		cell = sheet.getRow(1).getCell(4);
		driver.findElement(billingCity).clear();
		driver.findElement(billingCity).sendKeys(cell.getStringCellValue());

		Waits.pause(1);
		scrollAndClick(find(saveButton),false);
		Waits.pause(1);

	}

	public void pastCustomerInfo() throws Exception {
		selectPastCustomer();
		File src = new File("./Workbook1.xls");
		FileInputStream finput = new FileInputStream(src);
		workbook = new HSSFWorkbook(finput);
		sheet = workbook.getSheetAt(0);

		// Account Name
		cell = sheet.getRow(2).getCell(0);
		driver.findElement(accountName).clear();
		driver.findElement(accountName).sendKeys(cell.getStringCellValue());

		// Phone Number
		cell = sheet.getRow(2).getCell(1);
		driver.findElement(phoneField).clear();
		driver.findElement(phoneField).sendKeys(cell.getStringCellValue());

		// State
		cell = sheet.getRow(2).getCell(2);
		driver.findElement(billingState).clear();
		driver.findElement(billingState).sendKeys(cell.getStringCellValue());

		// Street
		cell = sheet.getRow(2).getCell(3);
		driver.findElement(billingStreet).clear();
		driver.findElement(billingStreet).sendKeys(cell.getStringCellValue());

		// City
		cell = sheet.getRow(2).getCell(4);
		driver.findElement(billingCity).clear();
		driver.findElement(billingCity).sendKeys(cell.getStringCellValue());

		Waits.pause(1);
		scrollAndClick(find(saveButton),false);
		Waits.pause(1);

	}

	// Verification of Excel Data
	public static String verifyOExcelAccountName() {
		String excelAccount = sheet.getRow(0).getCell(0).toString() +" "+formattedDate;
		return excelAccount;

	}

	public String verifyOExcelAccountPhone() {
		String excelPhoneNumber = sheet.getRow(0).getCell(1).toString();
		return excelPhoneNumber;

	}

	public String verifyOExcelAccountAddress() {
		String billingAddress = sheet.getRow(0).getCell(3).toString();
		return billingAddress;

	}

	public String verifyPExcelAccountName() {
		String excelAccount = sheet.getRow(1).getCell(0).toString();
		return excelAccount;

	}

	public String verifyPExcelAccountPhone() {
		String excelPhoneNumber = sheet.getRow(1).getCell(1).toString();
		return excelPhoneNumber;

	}

	public String verifyPExcelAccountAddress() {
		String billingAddress = sheet.getRow(1).getCell(3).toString();
		return billingAddress;

	}

	public String verifyPSTExcelAccountName() {
		String excelAccount = sheet.getRow(2).getCell(0).toString();
		return excelAccount;

	}

	public String verifyPSTExcelAccountPhone() {
		String excelPhoneNumber = sheet.getRow(2).getCell(1).toString();
		return excelPhoneNumber;

	}

	public String verifyPSTExcelAccountAddress() {
		String billingAddress = sheet.getRow(2).getCell(3).toString();
		return billingAddress;

	}

}
