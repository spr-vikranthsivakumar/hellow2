package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Waits;

public class RecentAccountsPage extends BasePage{
	
	public RecentAccountsPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	String title = "Accounts: Home ~ Salesforce - Enterprise Edition";
	
	public RecentAccountsPage waitForPage(WebDriver driver) {
		while (!driver.getTitle().contains(title)) {
			Waits.pause(1);
			System.out.println("Waiting for Recent Accounts Page page to load..." + driver.getTitle());
		}
		System.out.println("Sales Force Account Page Loaded");
		return PageFactory.initElements(driver, RecentAccountsPage.class);
	}
//Elements
	By mostRecentAccount = By.linkText(CreateAccountPage.verifyOExcelAccountName());

public RecentAccountsPage selectMostRecentAccount() {
	driver.findElement(mostRecentAccount).click();
	return null;
	}
}
