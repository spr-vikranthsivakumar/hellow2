package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Waits;


public class LandingPage extends BasePage{

	

	public LandingPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	private String title = "Salesforce - Enterprise Edition";
	
	public LandingPage waitForPage(WebDriver driver) {
		while (!driver.getTitle().contains(title)){
			Waits.pause(1);
			System.out.println("Waiting for Sales Force Landing page to load..." + driver.getTitle());
		}
		System.out.println("Sales Force Landing Page Loaded");
		return PageFactory.initElements(driver, LandingPage.class);
	}
	
	public String getURL(){
		String url = driver.getCurrentUrl();
		return url;
	}
	

	
	//Elements
	By createNewDropDown = By.id("createNewButton");
	By createNewDropdownContact = By.xpath("//*[@id=\"createNewMenu\"]/a[4]"); 

	public void createNewDropdownContact() throws Exception {
		driver.findElement(createNewDropDown).click();
		driver.findElement(createNewDropdownContact).click();
	}
	
}

