package pages;



import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Waits;

public class AccountDetailsPage extends BasePage {

	public AccountDetailsPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	String title = "Account: ";

	public AccountDetailsPage waitForPage(WebDriver driver) {
		while (!driver.getTitle().contains(title)) {
			Waits.pause(1);
			System.out.println("Waiting for Sales Force Account page to load..." + driver.getTitle());
		}
		System.out.println("Sales Force Account Page Loaded");
		return PageFactory.initElements(driver, AccountDetailsPage.class);
	}

	// Elements
	By accountName = By.cssSelector("h2.topName");
	By billingAddress = By.cssSelector("#acc17_ileinner>table>tbody>tr:nth-of-type(1)>td");
	By phoneNumber = By.cssSelector("#acc10_ileinner");

	By deleteButton = By.cssSelector("#topButtonRow>input:nth-of-type(4)");

	// Verifying Data on Account Details Page
	public String verifyAccountName() {
		String text = driver.findElement(accountName).getText();
		System.out.println("Account Name:" + text);
		return text;
	}

	public String verifyAccountPhoneNumber() {
		String text = driver.findElement(phoneNumber).getText();
		System.out.println("Phone Number:" + text);
		return text;
	}

	public String verifyAccountBillingAddress() {
		String text = driver.findElement(billingAddress).getText();
		System.out.println("Billing Address: " + text);
		return text;
	}

	// Delete Method returning Recent Accounts Page
	public RecentAccountsPage deleteAccount() {
		driver.findElement(deleteButton).click();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = driver.switchTo().alert();
		alert.accept();
		RecentAccountsPage rap = new RecentAccountsPage(driver).waitForPage(driver);
		return rap;
	}

}
