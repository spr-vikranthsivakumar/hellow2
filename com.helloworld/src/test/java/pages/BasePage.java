package pages;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.openqa.selenium.*;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Waits;


public class BasePage {
	public static WebDriver driver;

	static Date date = new Date();
	static SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy_h_mm_ss_a");
	static String formattedDate = sdf.format(date);
		
	HSSFWorkbook workbook;

	static HSSFSheet sheet;

	HSSFCell cell;

	private int DEFAULT_TIMEOUT = 30;

	protected String title;

	public BasePage(WebDriver driver) {
		this.driver = driver;
	}


	public void waitForUrlFragment(String urlFragment) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		try {
			wait.until(ExpectedConditions.urlContains(urlFragment));
			System.out.println(getClass().getSimpleName() + " Loaded \r");

		} catch (Exception e) {
			System.err.println(getClass().getSimpleName() + " Not Found \r");
		}
	}

	public void waitForTitle(String title) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		try {
			wait.until(ExpectedConditions.titleContains(title));
			System.out.println(getClass().getSimpleName() + " Loaded \r");

		} catch (Exception e) {
			System.err.println(getClass().getSimpleName() + " Not Found \r");
		}
	}

	public void waitForTitle(String expectedTitle, String pageClass, int timeout) {
		int count = 0;
		while (!driver.getTitle().contains(expectedTitle)) {
			Waits.pause(1);
			System.out.println("Waiting for " + pageClass + " page to load..." + driver.getTitle());
			count += 1;
			if (count >= timeout) {
				throw new TimeoutException("Timed out waiting for " + pageClass + " to load.");
			}
		}
		System.out.println(pageClass + " page loaded");
	}

	public void waitForTitle(String expectedTitle, String pageClass) {
		waitForTitle(expectedTitle, pageClass, DEFAULT_TIMEOUT);
	}

	public void waitForURL(String substring, String pageClass, int timeout) {
		int count = 0;
		while (!driver.getCurrentUrl().contains(substring)) {
			Waits.pause(1);
			System.out.println("Waiting for " + pageClass + " page to load..." + driver.getTitle());
			count += 1;
			if (count >= timeout) {
				throw new TimeoutException("Timed out waiting for " + pageClass + " to load.");
			}
		}
		System.out.println(pageClass + " page loaded");
	}

	public void waitForURL(String substring, String pageClass) {
		waitForURL(substring, pageClass);
	}

	public void waitForElement(By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			System.out.println(getClass().getSimpleName() + " Loaded \r");

		} catch (Exception e) {
			System.err.println(getClass().getSimpleName() + " Not Found \r");
		}
	}

	public WebElement find(By locator) {
		return driver.findElement(locator);
	}

	public List<WebElement> findAll(By locator) {
		return driver.findElements(locator);
	}

	public void scrollTo(WebElement e) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", e);
	}

	public void scrollX(By b) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 600);");
	}

	public void scrollY() {
		Waits.pause(5);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 600);");
	}

	public void scrollTo(By e) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(e));
	}

	public void hover(By locator) {
		WebElement e = find(locator);
		Actions builder = new Actions(driver);
		builder.moveToElement(e).build().perform();
	}

	/**
	 * Assumes user is logged in under the account to be deleted
	 */

	public void click(By locator) {
		find(locator).click();
	}

	public void type(String inputText, By locator) {
		find(locator).clear();
		find(locator).sendKeys(inputText);
	}

	public void type(int inputNumber, By locator) {
		find(locator).clear();
		find(locator).sendKeys(String.valueOf(inputNumber));
	}

	public void type(String inputText, WebElement e) {
		e.clear();
		e.sendKeys(inputText);
	}

	public void type(int inputNumber, WebElement e) {
		e.clear();
		e.sendKeys(String.valueOf(inputNumber));
	}

	public Boolean isDisplayed(By locator) {
		try {
			return find(locator).isDisplayed();
		} catch (NoSuchElementException exception) {
			return false;
		}
	}

	public Boolean waitForDisplayed(By locator, Integer... timeout) {
		try {
			waitFor(ExpectedConditions.visibilityOfElementLocated(locator), (timeout.length > 0 ? timeout[0] : null));
		} catch (org.openqa.selenium.TimeoutException exception) {
			return false;
		}
		return true;
	}

	private void waitFor(ExpectedCondition<WebElement> condition, Integer timeout) {
		timeout = timeout != null ? timeout : 7;
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(condition);
	}

	/**
	 * use where more robust method than type(by) is needed
	 *
	 * @param WebElement
	 *            textElement
	 * @param String
	 *            value
	 * @throws InvalidElementStateException
	 */
	private static void inputText(WebElement textElement, String value) throws InvalidElementStateException {

		if (!textElement.getAttribute("type").contentEquals("email")
				|| !textElement.getAttribute("type").contentEquals("password")
				|| !textElement.getAttribute("type").contentEquals("text")
				|| !textElement.getAttribute("type").contentEquals("search")) {
			throw new InvalidElementStateException("This web element is not a text input!");
		}
		// check if it is displayed and enabled
		if (textElement.isDisplayed() && textElement.isEnabled()) {
			textElement.click();
			textElement.clear();
			textElement.sendKeys(value);
		} else {
			throw new InvalidElementStateException(
					"Text input by " + textElement.getAttribute("id") + " is disabled or not displayed!");
		}
	}

	public String getText(By locator) {
		return find(locator).getText();
	}

	public List<String> getTexts(By locator) {
		List<String> texts = new ArrayList();
		List<WebElement> e = findAll(locator);
		for (WebElement w : e) {
			texts.add(w.getText());
		}
		return texts;
	}

	protected static void console(Object out) {
		System.out.println(out);
	}
	public static void scrollIntoView(WebElement elem, boolean top) {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(" + top + ");", elem);
	}
	public static void scrollAndClick(WebElement elem, boolean top) {
		scrollIntoView(elem, top);
		Waits.waitForClickable(driver, elem).click();
	}

}
