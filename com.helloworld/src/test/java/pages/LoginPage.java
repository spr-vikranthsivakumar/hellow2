package pages;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Waits;

public class LoginPage extends BasePage {

	HSSFWorkbook workbook;

	HSSFSheet sheet;

	HSSFCell cell;
	private String title = "Login | Salesforce";
	
	
	
	public LoginPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public LoginPage waitForPage(WebDriver driver) throws TimeoutException {
		waitForTitle(title);
		return PageFactory.initElements(driver, LoginPage.class);
	}
	
	
	//Elements
	By usernameField = By.id("username");
	By passwordField = By.id("password");
	By loginButton = By.id("Login");
	
	
	
	public void loginMethod(String user, String pass){
		type(user,usernameField);
		System.out.println("username: " + user);
		type(pass,passwordField);
		System.out.println("password: " + pass);
		
		click(loginButton);
		Waits.pause(2);
		
	}
	

	public void readData() throws Exception{
	File src = new File("./Test33.xls");
	FileInputStream finput = new FileInputStream(src);
	workbook = new HSSFWorkbook(finput);
	sheet = workbook.getSheetAt(0);
	
	cell = sheet.getRow(0).getCell(0);
	
	cell.setCellType(Cell.CELL_TYPE_STRING);
	driver.findElement(By.id("username")).sendKeys(cell.getStringCellValue());
	
	cell = sheet.getRow(0).getCell(1);
	cell.setCellType(Cell.CELL_TYPE_STRING);
	driver.findElement(By.id("password")).sendKeys(cell.getStringCellValue());
	driver.findElement(loginButton).click();
	Waits.pause(2);
	}

}


