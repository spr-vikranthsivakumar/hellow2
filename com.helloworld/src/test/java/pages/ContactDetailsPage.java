package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Waits;

public class ContactDetailsPage extends BasePage {

	public ContactDetailsPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	String title = "Contact: ";

	public ContactDetailsPage waitForPage(WebDriver driver) {
		while (!driver.getTitle().contains(title)) {
			Waits.pause(1);
			System.out.println("Waiting for Sales Force Contact page to load..." + driver.getTitle());
		}
		System.out.println("Sales Force Contact Details Page Loaded");
		return PageFactory.initElements(driver, ContactDetailsPage.class);
	}

	// Elements
	By fullName = By.cssSelector("h2.topName") ;
	By accountName = By.id("con4_ileinner");
	
	By deleteButton = By.cssSelector("#topButtonRow>input:nth-of-type(4)");

	// Verifying Data on Account Details Page
	public String verifyAccountName() {
		String text = driver.findElement(accountName).getText();
		System.out.println("Account Name:" + text);
		return text;
	}

	public String verifyFullName() {
		String text = driver.findElement(fullName).getText();
		System.out.println("Contact Full Name:" + text);
		return text;
	}

	// Delete Method returning Recent Contacts Page
	public RecentContactsPage deleteContact() {
		driver.findElement(deleteButton).click();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.alertIsPresent());
		Alert alert = driver.switchTo().alert();
		alert.accept();
		RecentContactsPage rap = new RecentContactsPage(driver).waitForPage(driver);
		return rap;
	}

}
