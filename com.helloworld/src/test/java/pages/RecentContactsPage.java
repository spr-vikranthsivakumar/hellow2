package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Waits;

public class RecentContactsPage extends BasePage{
	
	public RecentContactsPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	String title = "Contacts: Home ~ Salesforce - Enterprise Edition";
	
	public RecentContactsPage waitForPage(WebDriver driver) {
		while (!driver.getTitle().contains(title)) {
			Waits.pause(1);
			System.out.println("Waiting for Recent Contacts Page page to load..." + driver.getTitle());
		}
		System.out.println("Sales Force Recent Contacts Page Loaded");
		return PageFactory.initElements(driver, RecentContactsPage.class);
	}
//Elements
	By mostRecentContact = By.xpath("//*[@id=\"bodyCell\"]/div[3]/div[1]/div/div[2]/table/tbody/tr[2]/th/a");

public RecentAccountsPage selectMostRecentContact() {
	driver.findElement(mostRecentContact).click();
	return null;
	}
}