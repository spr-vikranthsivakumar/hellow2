package pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utilities.Waits;

public class CreateContactPage extends BasePage {

	public CreateContactPage(WebDriver driver) {
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	String title = "Contact Edit: New Contact ~ Salesforce - Enterprise Edition";

	public CreateContactPage waitForPage(WebDriver driver) {
		while (!driver.getTitle().contains(title)) {
			Waits.pause(1);
			System.out.println("Waiting for Sales Force Account page to load..." + driver.getTitle());
		}
		System.out.println("Sales Force Landing Page Loaded");
		return PageFactory.initElements(driver, CreateContactPage.class);
	}

	// Elements
	By saveButton = By.cssSelector("#bottomButtonRow>input:nth-of-type(1)");
	
	// Account Fields
	By firstName = By.id("name_firstcon2");
	By lastName = By.id("name_lastcon2");
	By accountName = By.id("con4");
	
	// Iteration for grabbing Data
	public void fillContactInfo() throws Exception {
		File src = new File("./Test33.xls");
		FileInputStream finput = new FileInputStream(src);
		workbook = new HSSFWorkbook(finput);
		sheet = workbook.getSheetAt(0);

		// First Name
		cell = sheet.getRow(0).getCell(6);
		cell.setCellType(Cell.CELL_TYPE_STRING);
		driver.findElement(firstName).clear();
		driver.findElement(firstName).sendKeys(cell.getStringCellValue());
		
		// Last Name
		cell = sheet.getRow(0).getCell(7);
		cell.setCellType(Cell.CELL_TYPE_STRING);
		driver.findElement(lastName).clear();
		driver.findElement(lastName).sendKeys(cell.getStringCellValue());
		
		// Account Name
		cell = sheet.getRow(0).getCell(0);
		cell.setCellType(Cell.CELL_TYPE_STRING);
		driver.findElement(accountName).clear();
		driver.findElement(accountName).sendKeys(cell.getStringCellValue()+" "+formattedDate);

		Waits.pause(1);
		driver.findElement(saveButton).click();
		Waits.pause(1);

	}

	// Verification of Excel Data
	public String verifyCExcelAccountName() {
		String excelAccount = sheet.getRow(0).getCell(0).toString()+" "+formattedDate;
		return excelAccount;

	}

	public String verifyCFullName() {
		String excelFullName = sheet.getRow(0).getCell(6).toString() + " " + sheet.getRow(0).getCell(7).toString();
		return excelFullName;

	}
}
