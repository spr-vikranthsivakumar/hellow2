package utilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import tests.BaseTest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static utilities.Utilities.console;

public class Config {
	
	private static WebDriver driver;
	public static String environment = "NA";
	
	public static WebDriver getDriver(){
		return driver;
	}
	
	public static String getWebsite() {
		return environment;
	}
	
	public static WebDriver setDriver(String s) {
		if(s.equals("chrome")){
			driver = new ChromeDriver();
			driver.manage().window().setSize(new Dimension(1680,1050));
		}
		if(s.equals("firefox")){

			driver = new FirefoxDriver();
			driver.manage().window().maximize();
		}
		if(s.equals("safari")){
			System.setProperty("webdriver.safari.noinstall", "true");
			driver = new SafariDriver();
		}
		if(s.equals("chromeandroidemu")) {
            DesiredCapabilities caps = new DesiredCapabilities();
			//caps.setCapability("appiumVersion", "1.5.3");
			caps.setCapability("browserName", "Chrome");
			caps.setCapability("deviceName", "192.168.56.101:5555");
			caps.setCapability("platformVersion", "5.1");
            caps.setCapability("platformName", "Android");
            try {
                URL appium = new URL("http://127.0.0.1:4723/wd/hub");
                driver = new AndroidDriver<WebElement>(appium, caps);
            }
			catch (MalformedURLException e) {console("oh no");}
        }
        if (s.equals("chromeandroiddevice")) {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("appiumVersion", "1.5.3");
            caps.setCapability("browserName", "Chrome");
            caps.setCapability("deviceName", "LGLS99014de698e");
            caps.setCapability("platformVersion", "5.0");
            caps.setCapability("platformName", "Android");
            try {
                URL appium = new URL("http://127.0.0.1:4723/wd/hub");
                driver = new AndroidDriver<WebElement>(appium, caps);
            }
            catch (MalformedURLException e) {console("oh no");}
        }
        if (s.equals("chromeemu")) {
            Map<String, String> mobileEmulation = new HashMap<String, String>();
            mobileEmulation.put("deviceName", "Google Nexus 6");

            Map<String, Object> chromeOptions = new HashMap<String, Object>();
            chromeOptions.put("mobileEmulation", mobileEmulation);
            DesiredCapabilities capabilities = DesiredCapabilities.chrome();
            capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
            driver = new ChromeDriver(capabilities);
        }
        if (s.equals("safarimobiledevice")) {
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("appiumVersion", "1.5.3");
            caps.setCapability("browserName", "Safari");
            caps.setCapability("deviceName", "");
            caps.setCapability("platformVersion", "9.3");
            caps.setCapability("platformName", "iOS");
            try {
                URL appium = new URL("http://127.0.0.1:4723/wd/hub");
                driver = new IOSDriver<WebElement>(appium, caps);
            }
            catch (MalformedURLException e) {console("oh no");}
        }
        if (s.equals("safariemu")) {
			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("appiumVersion", "1.5.3");
			caps.setCapability("browserName", "Safari");
			caps.setCapability("deviceName", "iPhone Simulator");
			caps.setCapability("platformVersion", "9.3");
			caps.setCapability("platformName", "iOS");
			try {
				URL appium = new URL("http://127.0.0.1:4723/wd/hub");
				driver = new IOSDriver<WebElement>(appium, caps);
			}
			catch (MalformedURLException e) {console("oh no");}
		}
		return driver;
	}

	public static void inSandBoxEnvironment() {
		environment="https://test.salesforce.com/";
		BaseTest.url=environment;
		driver.get(environment+"");
	}


}