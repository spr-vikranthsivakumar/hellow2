package utilities;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Dictionary;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import tests.BaseTest;

public class Utilities extends BaseTest{

	//static WebDriver driver;
	static WebDriverWait tock;
	private static WebElement e;

	/**
	* String  filename --> saves to test-output/screengrabs/filename.png
	* @param filename
	* @throws IOException
	*/
	public static void screenGrab(String filename) throws IOException{
		Waits.pause(1);
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("./screengrabs/"+filename+".png"));
	}
	
	

	public static char randomChar(){
		Random letter = new Random();
		return (char) (letter.nextInt(26) + 'a');
	}

	public static int randomDigit(){
		Random number = new Random();
		return number.nextInt(9);
	}

	public static Boolean matchFound(String patternValue, String value) {
		Pattern pattern = Pattern.compile(patternValue);
		Matcher matcher = pattern.matcher(value);
		return matcher.find();
	}

	public static String displayNamePatternValue(){return "[A-Z]{1}"+"[a-z]+"+"[\\s]{1}"+"[A-Z]{1}+$"; }

	public String getTitle(){
		return driver.getTitle();
	}

	public static String elementText(WebElement e){
		String s = e.getText();
		return s;
	}

	public static List<String> listOfElementsText(List<WebElement> elements){
		List<String> titles = new ArrayList<String>();

		for(WebElement e : elements){
		String s = elementText(e);
		titles.add(s);
		}
		return titles;
	}

	public static void sendToChildInput(WebElement elem, String text) {
		elem.findElement(By.tagName("input")).sendKeys(text);
	}

	public static void sendToChildTextArea(WebElement elem, String text) {
		elem.findElement(By.tagName("textarea")).sendKeys(text);
	}

	public boolean exists(WebElement elem) {
		try {
			new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOf(elem));
			return true;
		}
		catch (Exception ex) {
			return false;
		}
	}

	public boolean exists(By by) {
		if(driver.findElement(by).isDisplayed()){return true;}
		else return driver.findElements(by).size() > 0;
	}

	// checks to see if an element is displayed or not.
	public static boolean elementPresent(WebElement e) throws Exception, IOException, SQLException {
		try {
			e.isDisplayed();
			return true;
		}
		catch (Throwable t) {
			return false;
		}
	}

	/**
	* from http://santoshsarmajv.blogspot.in/ <br>
	* use this website to verify if date are output correctly http://www.timeanddate.com/date/dateadd.html <br>
	*
	* @param period - The date can be changed by  inserting negative or positive number. Negative number = past/later date
	* and positive number = future date. if today date is 11/19/2013, calling getDate(5, "MM/dd/yyyy")
	* is 5 days later from today so it will get 11/24/2013. enter -5 will get past date, so it will be 11/14/2013 <br>
	*
	* @param format - date format <br>
	*
	* @return
	* getDate(period, format);
	*
	*/
	public static String getDate(int period,String format) {
		Calendar currentDate = Calendar.getInstance();
		SimpleDateFormat formatter= new SimpleDateFormat(format);
		currentDate.add(Calendar.DAY_OF_MONTH, period);
		String date = formatter.format(currentDate.getTime());
		return date;
	}

	public static String getMethod() {
		final StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		String method = ste[2].getMethodName();
		return method;
	}
	
	
	/**
	* @param locator = By which identifies unique field that accepts a string input
	* @param keys = keys to be input into field
	* @Return e = WebElement with value(equal to keys) input into field
	*
	**/
	public void type(String inputText, By locator) {
		find(locator).clear();
		find(locator).sendKeys(inputText);
	}

	public void type(int inputNumber, By locator) {
		find(locator).clear();
		find(locator).sendKeys(String.valueOf(inputNumber));
	}

	public void type(String inputText, WebElement e) {
		e.clear();
		e.sendKeys(inputText);
	}

	public void type(int inputNumber, WebElement e) {
		e.clear();
		e.sendKeys(String.valueOf(inputNumber));
	}

	public static void scrollTo(WebElement e) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", e);
	}

	public static void scrollTo(By by) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(by));
	}

	public static void scrollIntoView(WebElement elem) {
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", elem);
		((JavascriptExecutor)driver).executeScript("scrollBy(0,-93);");
	}

	public void hover(By locator) {
		WebElement e = find(locator);
		Actions builder = new Actions(driver);
		builder.moveToElement(e).build().perform();
	}


	public WebElement find(By locator) {
		return driver.findElement(locator);
	}

	public WebElement find(WebElement e){
		try{
			e.isDisplayed();
			return e;
		}
		catch (Exception NoSuchElementException){
			System.err.println("Element not found");
		}
		return e;
	}

	public Boolean isDisplayed(By locator) {
		try {
			return find(locator).isDisplayed();
		}
		catch (org.openqa.selenium.NoSuchElementException exception) {
			return false;
		}
	}

	public void printText(By locator){
		System.out.println(find(locator).getText());
	}

	public String getText(By locator){
		return find(locator).getText();
	}

	public static void console(Object object) {
		System.out.println(object);
	}
}