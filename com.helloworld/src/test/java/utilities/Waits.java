package utilities;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.BasePage;
import tests.BaseTest;

public class Waits extends BaseTest{

    private static int DEFAULT_TIMEOUT = 30;
    private static String state;

    public static WebElement waitForVisible(WebDriver driver, By by, int timeout) {
        return new WebDriverWait(driver, timeout)
            .until (ExpectedConditions.visibilityOfElementLocated (by));
    }
    public static WebElement waitForVisible(WebDriver driver, By by) {
        return waitForVisible(driver, by, DEFAULT_TIMEOUT);
    }

    public static WebElement waitForVisible(WebDriver driver, WebElement elem, int timeout) {
        return new WebDriverWait(driver, timeout)
            .until (ExpectedConditions.visibilityOf(elem));
    }
    public static WebElement waitForVisible(WebDriver driver, WebElement elem) {
        return waitForVisible(driver, elem, DEFAULT_TIMEOUT);
    }

    public static List<WebElement> waitForVisibleList(WebDriver driver, By by, int timeout) {
        return new WebDriverWait (driver, timeout)
            .until (ExpectedConditions.visibilityOfAllElementsLocatedBy (by));
    }
    public static List<WebElement> waitForVisibleList(WebDriver driver, By by) {
        return waitForVisibleList(driver, by, DEFAULT_TIMEOUT);
    }

    public static void waitForNotVisible(WebDriver driver, By by, int timeout) {
        new WebDriverWait(driver, timeout)
            .until(ExpectedConditions.invisibilityOfElementLocated(by));
    }
    public static void waitForNotVisible(WebDriver driver, By by) {
        waitForNotVisible(driver, by, DEFAULT_TIMEOUT);
    }

    public static WebElement waitForClickable(WebDriver driver, By by, int timeout) {
        return new WebDriverWait (driver, timeout)
            .until (ExpectedConditions.elementToBeClickable (by));
    }
    public static WebElement waitForClickable(WebDriver driver, By by) {
        return waitForClickable(driver, by, DEFAULT_TIMEOUT);
    }

    public static WebElement waitForClickable(WebDriver driver, WebElement elem, int timeout) {
        return new WebDriverWait (driver, timeout)
            .until (ExpectedConditions.elementToBeClickable (elem));
    }
    public static WebElement waitForClickable(WebDriver driver, WebElement elem) {
        return waitForClickable(driver, elem, DEFAULT_TIMEOUT);
    }
    
    private static void waitFor(ExpectedCondition<WebElement> condition, Integer timeout, WebDriverWait wait) {
        timeout = timeout != null ? timeout : 5;
        wait = new WebDriverWait(driver, timeout);
        wait.until(condition);
    }

    public static void pause(int i) {

        long now = System.currentTimeMillis();
        long wait = i*1000;
        long until = now+wait;
        
        while(System.currentTimeMillis() < until){
            //do nothing, just wait
        }
	}
     

}
