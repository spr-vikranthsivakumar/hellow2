package utilities;

import java.util.ArrayList;
import java.util.Random;

public class Credentials {

	public static String userLogin;
	public static String userPassword;

	static String[] credentials = new String[2];

	public static String[] sandboxLogin(){return new String[] {"SPRautotesting@spr.com.sandbox", "Autosb2017!"};}
	
	public static String[] specialCharacter(){ return new String[] {"pecialchars@net.com", speciChars};}
	private static String speciChars = "@\":;\""+"!@#'$%^&*()-_+={}[]|\\,.<>/?`~";
	//public static String[] randomCredentials(){
		//return new String[] {randomEmail(), randomPassword()};
	
	
	public static void setCredentials(String[] cred){
		userLogin = cred[0];
		userPassword = cred[1];
		/*try {
			userID = cred[2];
		}
		catch (ArrayIndexOutOfBoundsException e) {
			userID = "";
		*/}
	

	public String[] getCredentials(){
		String[] creds = new String[2];
		credentials(userLogin, userPassword);
		return creds;
	}
	
	public static String[] credentials(String user, String pass){
		String[] creds = new String[2];
		creds[0] = user;
		creds[1] = pass;
		return creds;
	}

	public static void setUser(String s){
		userLogin = s;
	}
	
	public static void setPassword(String s){
		userPassword =s;
	}
	
	
	public static String userLogin(){
		return userLogin;
	}
	
	public static String userPassword(){
		return userPassword;
	}

	public static String randomEmail(){
		
		String extension;
		ArrayList<String> list = new ArrayList<String>();
		list.add(".com");
		list.add(".net");
		list.add(".edu");
		list.add(".org");
		list.add(".gov");
		list.add(".uk");
		list.add(".us");
		
		int i = list.size();
		int random = new Random().nextInt(i);
		int random2 = new Random().nextInt(i);
		int random3 = new Random().nextInt(i);
		extension = list.get(random);
		
		return randomFirstName()+Integer.toString(random)+Integer.toString(random2)+Integer.toString(random3)+"@"
			+randomLastName()+Integer.toString(random)+Integer.toString(random2)
			+Integer.toString(random3)+extension;
		
	}
	

	//Returns a random name from a list of common first names
		public static String randomFirstName(){
			
			int i;
			String name;
			ArrayList<String> list = new ArrayList<String>();
			list.add("John");
			list.add("William");
			list.add("Benjamin");
			list.add("Robert");
			list.add("David");
			list.add("Noah");
			list.add("Liam");
			list.add("Ryan");
			list.add("Bryan");
			list.add("Ethan");
			list.add("Eric");
			list.add("Alexander");
			list.add("Jason");
			list.add("Joshua");
			list.add("Neil");
			list.add("Michael");
			list.add("Alejandro");
			list.add("Dylan");
			list.add("James");
			list.add("Jose");
			list.add("Javiar");
			list.add("James");
			list.add("Matthew");
			list.add("Muhammad");
			list.add("Martin");
			list.add("Joseph");
			list.add("Richard");
			list.add("Charles");
			list.add("Thomas");
			list.add("Nicholas");
			list.add("Jack");
			list.add("Sebastian");
			list.add("Mary");
			list.add("Patricia");
			list.add("Linda");
			list.add("Barbara");
			list.add("Elizabeth");
			list.add("Jennifer");
			list.add("Sophia");
			list.add("Sarah");
			list.add("Emily");
			list.add("Olivia");
			list.add("Emma");
			list.add("Miriam");
			list.add("Maria");
			list.add("Margaret");
			list.add("Dorothy");
			list.add("Rachel");
			list.add("Ashley");
			list.add("Julia");
			list.add("Angela");
			list.add("Zoe");
			list.add("Mia");
			list.add("Isaac");
			
			i = list.size();
			int random = new Random().nextInt(i);
			name = list.get(random);
			System.out.println("Random Name: " + name);
			return name;
		}
		
	//Returns a random name from a list of common surnames 
		public static String randomLastName(){
			int i;
			String name;
			ArrayList<String> list = new ArrayList<String>();
			list.add("Jones");
			list.add("Smith");
			list.add("Williams");
			list.add("Stewart");
			list.add("Martinson");
			list.add("Cohen");
			list.add("Levy");
			list.add("Hogan");
			list.add("Patel");
			list.add("Kennedy");
			list.add("Davis");
			list.add("Miller");
			list.add("Taylor");
			list.add("Anderson");
			list.add("White");
			list.add("Jackson");
			list.add("Garcia");
			list.add("Martinez");
			list.add("Lee");
			list.add("Lewis");
			list.add("Walker");
			list.add("Hall");
			list.add("Allen");
			list.add("Young");
			list.add("King");
			list.add("Lopez");
			
			i = list.size();
			int random = new Random().nextInt(i);
			name = list.get(random);
			System.out.println("Random Last Name: " + name);
			return name;
		}
		
		
}
